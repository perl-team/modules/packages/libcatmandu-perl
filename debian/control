Source: libcatmandu-perl
Section: perl
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 libany-uri-escape-perl <!nocheck>,
 libapp-cmd-perl <!nocheck>,
 libasa-perl <!nocheck>,
 libcgi-expand-perl <!nocheck>,
 libcapture-tiny-perl <!nocheck>,
 libclass-method-modifiers-perl <!nocheck>,
 libclone-perl <!nocheck>,
 libconfig-onion-perl <!nocheck>,
 libcpanel-json-xs-perl <!nocheck>,
 libdata-compare-perl <!nocheck>,
 libdata-util-perl <!nocheck>,
 libhash-merge-simple-perl <!nocheck>,
 libhttp-message-perl <!nocheck>,
 libio-handle-util-perl <!nocheck>,
 liblist-someutils-perl (>= 0.59) <!nocheck>,
 liblog-any-adapter-log4perl-perl <!nocheck>,
 liblog-any-perl <!nocheck>,
 liblog-log4perl-perl <!nocheck>,
 libmime-types-perl <!nocheck>,
 libmodule-build-perl,
 libmodule-info-perl <!nocheck>,
 libmoo-perl <!nocheck>,
 libmoox-aliases-perl <!nocheck>,
 libnamespace-clean-perl <!nocheck>,
 libpackage-stash-perl <!nocheck>,
 libparser-mgc-perl (>= 0.21) <!nocheck>,
 libpath-iterator-rule-perl <!nocheck>,
 libpath-tiny-perl <!nocheck>,
 librole-tiny-perl <!nocheck>,
 libstring-camelcase-perl <!nocheck>,
 libsub-exporter-perl <!nocheck>,
 libtest-deep-perl <!nocheck>,
 libtest-exception-perl <!nocheck>,
 libtest-lwp-useragent-perl <!nocheck>,
 libtest-pod-perl <!nocheck>,
 libtext-csv-perl <!nocheck>,
 libtext-hogan-perl <!nocheck>,
 libthrowable-perl <!nocheck>,
 libtry-tiny-byclass-perl <!nocheck>,
 liburi-perl <!nocheck>,
 liburi-template-perl <!nocheck>,
 libuuid-tiny-perl <!nocheck>,
 libwww-perl <!nocheck>,
 libyaml-libyaml-perl <!nocheck>,
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders:
 Jonas Smedegaard <dr@jones.dk>,
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libcatmandu-perl.git
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libcatmandu-perl
Homepage: https://librecat.org/Catmandu/
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-perl

Package: libcatmandu-perl
Architecture: all
Depends:
 libany-uri-escape-perl,
 libapp-cmd-perl,
 libasa-perl,
 libcgi-expand-perl,
 libclass-method-modifiers-perl,
 libclone-perl,
 libconfig-onion-perl,
 libcpanel-json-xs-perl,
 libdata-compare-perl,
 libdata-util-perl,
 libhash-merge-simple-perl,
 libhttp-message-perl,
 libio-handle-util-perl,
 liblist-someutils-perl (>= 0.59),
 liblog-any-perl,
 libmime-types-perl,
 libmodule-info-perl,
 libmoo-perl,
 libmoox-aliases-perl,
 libnamespace-clean-perl,
 libpackage-stash-perl,
 libparser-mgc-perl (>= 0.21),
 libpath-iterator-rule-perl,
 libpath-tiny-perl,
 librole-tiny-perl,
 libstring-camelcase-perl,
 libsub-exporter-perl,
 libtext-csv-perl,
 libtext-hogan-perl,
 libthrowable-perl,
 libtry-tiny-byclass-perl,
 liburi-perl,
 liburi-template-perl,
 libuuid-tiny-perl,
 libwww-perl,
 libyaml-libyaml-perl,
 ${misc:Depends},
 ${perl:Depends},
Suggests:
 liblog-any-adapter-log4perl-perl,
 liblog-log4perl-perl,
Description: metadata toolkit
 Catmandu provides a suite of Perl modules
 to ease the import, storage, retrieval, export
 and transformation of metadata records.
 .
 Combine Catmandu modules
 with web application frameworks such as PSGI/Plack,
 document stores such as MongoDB or CouchDB
 and full text indexes as ElasticSearch or Solr
 to create a rapid development environment
 for digital library services
 such as institutional repositories and search engines.
 .
 Use of the debug option
 requires the packages liblog-any-adapter-log4perl-perl
 and liblog-log4perl-perl.
